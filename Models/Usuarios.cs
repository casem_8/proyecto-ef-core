﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoEntityFramework.Models
{
    public class Usuarios
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdUsuario { get; set; }

        [Required, StringLength(255)]
        public string Nombre { get; set; }

        [Required, StringLength(255)]
        public string Correo { get; set; }

        [Required, StringLength(10)]
        public string Telefono { get; set; }

        [Required, StringLength(255)]
        public string Direccion { get; set; }
    }
}
