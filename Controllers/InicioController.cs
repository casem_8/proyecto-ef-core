﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProyectoEntityFramework.Contexto;
using ProyectoEntityFramework.Models;
using System.Diagnostics;

namespace ProyectoEntityFramework.Controllers
{
    public class InicioController : Controller
    {
        // Inyectamos una instancia de MyDbContext
        private readonly MyDbContext _contexto;

        public InicioController(MyDbContext contexto)
        {
            _contexto = contexto;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            // Consulta para listar los usuarios de la BD
            var usuarios = await (from usuario in _contexto.Usuarios
                                  select usuario).ToListAsync();
            return View(usuarios);
        }
        
        [HttpGet]
        public IActionResult Crear()
        {
            return View();
        }

        // Crear usuario
        [HttpPost]
        public async Task<IActionResult> Crear(Usuarios usuarios)
        {
            if (usuarios != null)
            {
                _contexto.Usuarios.Add(usuarios);
                await _contexto.SaveChangesAsync();
                return RedirectToAction("Index");
            } 

            return View();  
        }

        [HttpGet]
        public IActionResult Editar(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuario = _contexto.Usuarios.Find(id);
            if (usuario == null)
            {
                return NotFound();
            }

            return View(usuario);
        }

        // Editar usuario
        [HttpPost]
        public async Task<IActionResult> Editar(Usuarios usuarios)
        {
            if (usuarios != null)
            {
                _contexto.Update(usuarios);
                await _contexto.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpGet]
        public IActionResult Eliminar(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuario = _contexto.Usuarios.Find(id);
            if (usuario == null)
            {
                return NotFound();
            }

            return View(usuario);
        }

        // Eliminar usuario
        [HttpPost, ActionName("Eliminar")]
        public async Task<IActionResult> EliminarUsuario(int? id)
        {
            var usuario = await _contexto.Usuarios.FindAsync(id);
            if (usuario == null)
            {
                return View();
            }

            _contexto.Usuarios.Remove(usuario);
            await _contexto.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}