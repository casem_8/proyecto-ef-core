﻿using Microsoft.EntityFrameworkCore;
using ProyectoEntityFramework.Models;

namespace ProyectoEntityFramework.Contexto
{
    public class MyDbContext : DbContext
    {
        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
        {
            
        }

        // Entidad Usuarios aqui
        public DbSet<Usuarios> Usuarios { get; set; }
    }
}
